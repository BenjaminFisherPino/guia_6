import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		HashSet<Persona> estudiantes = new HashSet<>();
		HashSet<Persona> estudiantes2020 = new HashSet<>();	
		
		Scanner sc = new Scanner(System.in);
		String nombre;
		Persona a = new Estudiante("Joaquin Valenzuela");
		Persona b = new Estudiante("Sebastian Bravo");
		Persona c = new Estudiante("Sebastian Farias");			
		Persona d = new Estudiante("Agustin Gonzales");
		Persona e = new Estudiante("Agustin Rodriguez");
		Persona f = new Estudiante("Cristobal Becerra");
		
		estudiantes.add(a);
		estudiantes.add(b);
		estudiantes.add(c);		
		estudiantes2020.add(d);
		estudiantes2020.add(e);
		estudiantes2020.add(f);
		
		
		//SE IMPRIME LA LISTA DE ESTUDIANTES
		System.out.println("Lista de estudiantes: \n");
		Iterator<Persona> iterate = estudiantes.iterator();
		while(iterate.hasNext()) {
			System.out.println(iterate.next().get_nombre());
			}
		System.out.println("\n");
		
		//SE CONSULTA POR UN ALUMNO EN ESPECIFICO
		System.out.println("Ingrese el nombre del estudiante: \n");
		nombre = sc.nextLine();
		Iterator<Persona> iterate2 = estudiantes.iterator();
		while(iterate2.hasNext()) {
			if(iterate2.next().get_nombre().equals(nombre)) {
				System.out.println("/bien/");
			}
		}
		
		//SE IMPRIME LA LISTA DE ESTUDIANTES 2020
		System.out.println("Lista de estudiantes 2020: \n");
		Iterator<Persona> iterate2020 = estudiantes2020.iterator();
		while(iterate2020.hasNext()) {
			System.out.println(iterate2020.next().get_nombre());
			}
		System.out.println("\n");

		//SE COSULTA SI LOS ELEMENTOS DE ESTUDIANTE EXISTEN EN ESTUDINATES 2020
		if(estudiantes.containsAll(estudiantes2020)) {
			System.out.println("Hay estudiantes repetidos");
		}
		
		else {
			System.out.println("No hay estudiantes repetidos en las listas");
		}
		
		//UNION DEL CONJUNTO ESTUDIANTES CON EL CONJUNTO ESTUDINATES 2020
		estudiantes.addAll(estudiantes2020);
		
		//SE IMPRIME LA LISTA ACTUALIZADA
		System.out.println("Lista de estudiantes actualizada: \n");
		Iterator<Persona> iterate3 = estudiantes.iterator();
		while(iterate3.hasNext()) {
			System.out.println(iterate3.next().get_nombre());
			}
		System.out.println("\n");
		
		//ELIMINA LOS ESTUDIANTES DEL CONJUNTO 2020 QUE NO PERTENEZCAN A ESTUDINATES
		estudiantes.removeAll(estudiantes2020);
		
		//SE IMPRIME LA LISTA PARA VERIFICAR
		Iterator<Persona> iterate4 = estudiantes.iterator();
		while(iterate4.hasNext()) {
			System.out.println(iterate4.next().get_nombre());
			}
		
	}

}
